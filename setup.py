from distutils.core import setup

setup(
    name='qc-fridge',
    version='0.0.1',
    description='QC Fridge Monitoring',
    author='Noah Goldman',
    author_email='noahg34@gmail.com',
 )
